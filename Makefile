DOCKER_REGISTRY_HOST?=registry.gitlab.com

DPROTO_NAMESPACE?=zalleon
DPROTO_APP?=protoc-gen
DPROTO_RELEASE?=v0.1.2-fix1

DPROTO_IMAGE?=${DOCKER_REGISTRY_HOST}/${DPROTO_NAMESPACE}/${DPROTO_APP}:${DPROTO_RELEASE}
DPROTO?=docker run --rm -v $(shell pwd):/defs -i ${DPROTO_IMAGE}

.PHONY: create
create:
	@echo "+ $@"
	if [ "${p}" == "" ]; then \
		echo "p is undefined"; \
	else \
		mkdir -p $$(dirname ./proto/$(p)); \
		${DPROTO} prototool create ./proto/$(p); \
	fi

.PHONY: lint
lint:
	@echo "+ $@"
	find ./proto -name "*.proto" -exec \
		sh -c ' \
			ecode=0; \
			for fname; do \
				echo "lint:" $$fname; \
				${DPROTO} prototool lint \
				--protoc-bin-path=/usr/local/bin/protoc \
				--protoc-wkt-path=/usr/local/include \
				$$fname; \
				ecode=$$(($$ecode+$$?)); \
			done; \
			exit $$(( $$ecode > 255 ? 255 : $$ecode )) \
		' sh {} +

.PHONY: gen-clients
gen-clients: gen-client-go

.PHONY: gen-client-go
gen-client-go:
	@echo "+ $@"
	rm -rf ./client/go && mkdir ./client/go
	find ./proto -name "*.proto" \
	-exec echo 'gen-client-go:' {} \; \
	-exec ${DPROTO} protoc \
		-I=/usr/local/include \
		--proto_path=./proto/ \
		--go_out=plugins=grpc:./client/go/ \
		--grpc-gateway_out=logtostderr=true:./client/go \
		--plugin=protoc-gen-go=/usr/local/bin/protoc-gen-go \
		--plugin=protoc-gen-grpc-gateway=/usr/local/bin/protoc-gen-grpc-gateway \
		{} \;
