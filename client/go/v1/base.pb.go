// Code generated by protoc-gen-go. DO NOT EDIT.
// source: v1/base.proto

package playgroundOrderSDKv1

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	any "github.com/golang/protobuf/ptypes/any"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type StatusCode int32

const (
	StatusCode_INVALID        StatusCode = 0
	StatusCode_OK             StatusCode = 1
	StatusCode_INTERNAL_ERROR StatusCode = 13
)

var StatusCode_name = map[int32]string{
	0:  "INVALID",
	1:  "OK",
	13: "INTERNAL_ERROR",
}

var StatusCode_value = map[string]int32{
	"INVALID":        0,
	"OK":             1,
	"INTERNAL_ERROR": 13,
}

func (x StatusCode) String() string {
	return proto.EnumName(StatusCode_name, int32(x))
}

func (StatusCode) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_7cadb02d0b0a23f3, []int{0}
}

type Status struct {
	Code                 StatusCode `protobuf:"varint,1,opt,name=code,proto3,enum=playgroundOrderSDK.v1.StatusCode" json:"code,omitempty"`
	Message              string     `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	Details              []*any.Any `protobuf:"bytes,3,rep,name=details,proto3" json:"details,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *Status) Reset()         { *m = Status{} }
func (m *Status) String() string { return proto.CompactTextString(m) }
func (*Status) ProtoMessage()    {}
func (*Status) Descriptor() ([]byte, []int) {
	return fileDescriptor_7cadb02d0b0a23f3, []int{0}
}

func (m *Status) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Status.Unmarshal(m, b)
}
func (m *Status) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Status.Marshal(b, m, deterministic)
}
func (m *Status) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Status.Merge(m, src)
}
func (m *Status) XXX_Size() int {
	return xxx_messageInfo_Status.Size(m)
}
func (m *Status) XXX_DiscardUnknown() {
	xxx_messageInfo_Status.DiscardUnknown(m)
}

var xxx_messageInfo_Status proto.InternalMessageInfo

func (m *Status) GetCode() StatusCode {
	if m != nil {
		return m.Code
	}
	return StatusCode_INVALID
}

func (m *Status) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *Status) GetDetails() []*any.Any {
	if m != nil {
		return m.Details
	}
	return nil
}

type ResponseMeta struct {
	Status               *Status  `protobuf:"bytes,1,opt,name=status,proto3" json:"status,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ResponseMeta) Reset()         { *m = ResponseMeta{} }
func (m *ResponseMeta) String() string { return proto.CompactTextString(m) }
func (*ResponseMeta) ProtoMessage()    {}
func (*ResponseMeta) Descriptor() ([]byte, []int) {
	return fileDescriptor_7cadb02d0b0a23f3, []int{1}
}

func (m *ResponseMeta) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ResponseMeta.Unmarshal(m, b)
}
func (m *ResponseMeta) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ResponseMeta.Marshal(b, m, deterministic)
}
func (m *ResponseMeta) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ResponseMeta.Merge(m, src)
}
func (m *ResponseMeta) XXX_Size() int {
	return xxx_messageInfo_ResponseMeta.Size(m)
}
func (m *ResponseMeta) XXX_DiscardUnknown() {
	xxx_messageInfo_ResponseMeta.DiscardUnknown(m)
}

var xxx_messageInfo_ResponseMeta proto.InternalMessageInfo

func (m *ResponseMeta) GetStatus() *Status {
	if m != nil {
		return m.Status
	}
	return nil
}

func init() {
	proto.RegisterEnum("playgroundOrderSDK.v1.StatusCode", StatusCode_name, StatusCode_value)
	proto.RegisterType((*Status)(nil), "playgroundOrderSDK.v1.Status")
	proto.RegisterType((*ResponseMeta)(nil), "playgroundOrderSDK.v1.ResponseMeta")
}

func init() { proto.RegisterFile("v1/base.proto", fileDescriptor_7cadb02d0b0a23f3) }

var fileDescriptor_7cadb02d0b0a23f3 = []byte{
	// 277 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x7c, 0x8f, 0xc1, 0x4a, 0xb4, 0x50,
	0x14, 0xc7, 0x3f, 0x67, 0x3e, 0x1c, 0x3a, 0x36, 0x83, 0x5c, 0x66, 0xc0, 0x82, 0xc0, 0x66, 0x25,
	0x2d, 0xae, 0x68, 0xf8, 0x00, 0xd6, 0xb8, 0x90, 0x99, 0x34, 0xee, 0xc4, 0x2c, 0x22, 0x88, 0x6b,
	0x9e, 0x24, 0x30, 0xaf, 0x78, 0x55, 0xf0, 0x11, 0x7a, 0xb5, 0x9e, 0x2a, 0xd0, 0xa4, 0xc5, 0x44,
	0xcb, 0x73, 0xf8, 0x9d, 0xff, 0xf9, 0xfd, 0x61, 0xde, 0x3a, 0x76, 0xc2, 0x25, 0xd2, 0xb2, 0x12,
	0xb5, 0x20, 0xab, 0x32, 0xe7, 0x5d, 0x56, 0x89, 0xa6, 0x48, 0xe3, 0x2a, 0xc5, 0x6a, 0xbf, 0xd9,
	0xd2, 0xd6, 0x39, 0x3f, 0xcb, 0x84, 0xc8, 0x72, 0xb4, 0x7b, 0x28, 0x69, 0x5e, 0x6d, 0x5e, 0x74,
	0xc3, 0xc5, 0xfa, 0x43, 0x01, 0x75, 0x5f, 0xf3, 0xba, 0x91, 0xc4, 0x83, 0xff, 0x2f, 0x22, 0x45,
	0x43, 0x31, 0x15, 0x6b, 0xe1, 0x5e, 0xd2, 0x5f, 0xb3, 0xe8, 0x00, 0xdf, 0x8a, 0x14, 0x59, 0x8f,
	0x13, 0x03, 0x66, 0xef, 0x28, 0x25, 0xcf, 0xd0, 0x98, 0x98, 0x8a, 0x75, 0xc2, 0xc6, 0x91, 0x50,
	0x98, 0xa5, 0x58, 0xf3, 0xb7, 0x5c, 0x1a, 0x53, 0x73, 0x6a, 0x69, 0xee, 0x92, 0x0e, 0x22, 0x74,
	0x14, 0xa1, 0x7e, 0xd1, 0xb1, 0x11, 0x5a, 0x07, 0x70, 0xca, 0x50, 0x96, 0xa2, 0x90, 0x78, 0x87,
	0x35, 0x27, 0x1e, 0xa8, 0xb2, 0xff, 0xd6, 0x2b, 0x69, 0xee, 0xc5, 0x9f, 0x4a, 0xec, 0x1b, 0xbe,
	0xf2, 0x00, 0x7e, 0x24, 0x89, 0x06, 0xb3, 0x30, 0x3a, 0xf8, 0xbb, 0x70, 0xa3, 0xff, 0x23, 0x2a,
	0x4c, 0xe2, 0xad, 0xae, 0x10, 0x02, 0x8b, 0x30, 0x7a, 0x08, 0x58, 0xe4, 0xef, 0x9e, 0x03, 0xc6,
	0x62, 0xa6, 0xcf, 0x6f, 0xe8, 0xe3, 0xf2, 0x38, 0xbe, 0x75, 0x3e, 0x27, 0xab, 0xfb, 0xa3, 0xf5,
	0xd3, 0xc1, 0x49, 0xd4, 0xbe, 0xc4, 0xf5, 0x57, 0x00, 0x00, 0x00, 0xff, 0xff, 0x13, 0x25, 0xdb,
	0xf0, 0x83, 0x01, 0x00, 0x00,
}
